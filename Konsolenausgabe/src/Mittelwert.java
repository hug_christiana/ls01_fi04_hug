import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte x und y festlegen:
      // ===========================
      double summe;
      int anzahl;
      double mittelwert;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.print("Bitte geben Sie die Anzahl der zu addierenden Zahlen ein: ");
      anzahl = scan.nextInt();
      
      summe = 0;
  
      for (int count = 0; count < anzahl; count++) {
	      System.out.print("Bitte geben Sie eine Zahl ein: ");
	      double y = scan.nextDouble();
	      summe = summe + y;
      }
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      mittelwert = summe / anzahl;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert ist %.2f\n", mittelwert);
   }
}
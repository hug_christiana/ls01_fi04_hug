
public class Temperaturausgabe
{

	public static void main(String[] args) {
			
			//Aufgabe3
			
			System.out.printf("%-12s|%10s\r", "Fahrenheit", "Celsius");
			System.out.printf("------------------------\r");
			System.out.printf("%+-12d|%10.2f\r", -20, -28.8889);
			System.out.printf("%+-12d|%10.2f\r", -10, -23.3333);
			System.out.printf("%+-12d|%10.2f\r", 0, -17.7778);
			System.out.printf("%+-12d|%10.2f\r", 20, -6.6667);
			System.out.printf("%+-12d|%10.2f\r", 30, -1.1111);
			
	        /*
	        System.out.printf("%s  |   %s\r", "Fahrenheit", "Celsius");
			System.out.printf("------------------------\r");
			System.out.printf("%d         |%10.2f\r", -20, -28.8889);
			System.out.printf("%d         |%10.2f\r", -10, -23.3333);
			System.out.printf("+%d          |%10.2f\r", 0, -17.7778);
			System.out.printf("+%d         |%10.2f\r", 20, -6.6667);
			System.out.printf("+%d         |%10.2f\r", 30, -1.1111);
			*/
		}
}

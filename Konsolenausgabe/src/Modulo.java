
public class Modulo
{

	public static void main(String[] args)
	{
		int testNumber = 1; 
		
		while (testNumber <= 200) {
			
			int divisible = testNumber % 7;
			int divisible2 = testNumber % 5;
			int divisible3 = testNumber % 4;
			
			if (divisible == 0 && divisible2 != 0 && divisible3 == 0) {
				System.out.println (testNumber);
				testNumber++;
			}
			else
				testNumber++;
		}

	}

}
